/*la base des personnages */

class Personnage{
  /*stats en commun joueur, monstre*/
  constructor(nom,attaque,defence,vitesse,xp){
    this.nom = nom;
    this.attaque = attaque;
    this.defence = defence;
    this.vitesse = vitesse;
    this.xp = xp;
  }
/*action en commun joueur,monstre*/
}

class Joueur extends Personnage{

  /*stats joueur*/
  constructor(fain,soif,sex,bonneur,besoin){
    this.fain = fain;
    this.soif = soif;
    this.sex = sex;
    this.bonneur = bonneur;
    this.besoin = besoin;
  }
  /*interaction joueur or combat*/
}


